// import * as flsFunctions from './modules/functions.js';

// flsFunctions.isWebp();


// Custom JS
// input mask
// $("[name=PHONE]").inputmask('+9 (999) 999-99-99');
// $('[name="EMAIL"]').inputmask({
//     mask: "*{1,64}[.*{1,64}][.*{1,64}][.*{1,63}]@-{1,63}.-{1,63}[.-{1,63}][.-{1,63}]",
//     greedy: !1,
//     onBeforePaste: function(pastedValue, opts) {
//         pastedValue = pastedValue.toLowerCase();
//         return pastedValue.replace("mailto:", "");
//     },
//     definitions: {
//         '*': {
//             validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
//             cardinality: 1,
//             casing: "lower"
//         },
//         "-": {
//             validator: "[0-9A-Za-z-]",
//             cardinality: 1,
//             casing: "lower"
//         }
//     }
// });

const isNotMobile = $(window).width() + calcScroll() > 768;
const isNotTablet = $(window).width() + calcScroll() > 1024;

// избегание дергания при открытии окна
function calcScroll() {
  let div = document.createElement('div');

  div.style.width = '50px';
  div.style.height = '50px';
  div.style.overflowY = 'scroll';
  div.style.visibility = 'hidden';

  document.body.appendChild(div);
  let scrollWidth = div.offsetWidth - div.clientWidth;
  div.remove();

  return scrollWidth;
}

function hoverActive(selector) {
  $(selector).on('mouseover', function() {
    $(selector).removeClass('active');
    $(this).addClass('active');
  });
};

function clickActive(selector) {
  $(selector).click(function() {
    $(selector).removeClass('active');
    $(this).addClass('active');
  });
};

function changeTabs(item, content) {
  $(item).click(function() {
    $(item).removeClass('active').eq($(this).index()).addClass('active');
    $(content).hide().eq($(this).index()).fadeIn(1000);
  });
}

// apex charts
function createChartOptions(percents, months) {
  const monthsFontFaze = isNotMobile ? '12px' : '8px';

  return {
    series: [{
      name: 'Значение в процентах',
      data: percents,
    }],
      chart: {
      type: 'area',
      height: chartHeight,
      zoom: {
        enabled: false
      }
    },
    stroke: {
      show: true,
      curve: 'smooth',
      lineCap: 'butt',
      colors: ['#002C50'],
      width: 4,
      dashArray: 0,      
    },
    tooltip: {
      enabled: false,
    },
    fill: {
      colors: ['#F6F7FA'],
      opacity: 1,
    },
    dataLabels: {
      enabled: true,
      style: {
        colors: ['#1B3050']
      }
    },
    // subtitle: {
    //   text: 'Накопленная доходность',
    //   align: 'right',
    //   style: {
    //     colors: '#9399A4',
    //     fontSize: '14px',
    //     fontFamily: 'Montserrat, sans-serif',
    //     fontWeight: 400,
    //   },
    //   offsetY: 0,
    // },
    xaxis: {
      categories: months,
      labels: {
        style: {
          colors: '#9399A4',
          fontSize: monthsFontFaze,
          fontFamily: 'Montserrat, sans-serif',
          fontWeight: 400,
        },
        offsetX: 4,
      },
    },
    yaxis: {
      show: false,
      labels: {
        formatter: (value) => { return `${value}%` },
        style: {
          colors: '#9399A4',
          fontSize: '14px',
          fontFamily: 'Montserrat, sans-serif',
          fontWeight: 400,
        },
      }
    },
    legend: {
      horizontalAlign: 'left'
    }
  };
}

// chart options
const chartHeight = isNotMobile ? $(window).height() * 0.6 : $(window).height() * 0.3;
const chart = new ApexCharts(document.querySelector("#chart"), 
  createChartOptions(
    [`100`, `101.1`, `109.7`, `112.2`, `121.1`, `127.6`, `128.8`, `127.1`],
    [`сен. '16`, `дек. '16`, `дек. '17`, `дек. '18`, `дек. '19`, `дек. '20`, `дек. '21`, `мар. '22`]
  )
);
chart.render();
const chart_2 = new ApexCharts(document.querySelector("#chart_2"), 
  createChartOptions(
    [`100`, `102.2`, `104.6`, `109.2`, `112.4`, `110.4`, `108.7`, `109.0`, `107.1`],
    [`июл. '20`, `сен. '20`, `дек. '20`, `мар. '21`, `июн. '21`, `авг. '21`, `окт. '21`, `янв. 22`, `мар. 22`]
  )
);
chart_2.render();
const chart_3 = new ApexCharts(document.querySelector("#chart_3"), 
  createChartOptions(
    [`100`, `102.4`, `104.9`, `107.0`, `109.3`, `111.2`, `113.0`],
    [`авг. '20`, `дек. '20`, `мар. '21`, `июн. '21`, `сен. '21`, `дек. '21`, `мар. '22`]
  )
);
chart_3.render();
const allCharts = [chart, chart_2, chart_3];

// fullpage scroll
// let progressBar;
// const totalSections = $('.section').length;
const totalAnchors = document.querySelectorAll('.right-menu .rmc-point');
function addAnchorActive(index) {
  totalAnchors.forEach((point, i) => {
    for(let i = 0; i <= index; i++) {
      totalAnchors[i].classList.add('active');
    }
  });
}

$('#fullpage').fullpage({
  // menu: '#navMenu',
  // lockAnchors: true,
  scrollingSpeed: 700,
  autoScrolling: true,
  scrollHorizontally: true,
  onLeave: (origin, destination, direction, trigger) => {
    let idx = destination.index;
    // console.log('index:', idx);
    $('.rmc-point').removeClass('active');
    
    if(idx >= 4 && idx < 6) {
      addAnchorActive(idx + 1);
    } else if(idx >= 6 && idx < 8) {
      addAnchorActive(idx + 2);
    } else if(idx >= 8 && idx < 9) {
      addAnchorActive(idx + 1);
    } else if(idx >= 9 && idx < 11) {
      addAnchorActive(idx + 2);
    } else if(idx >= 11 && idx < 12) {
      addAnchorActive(idx + 3);
    } else if(idx >= 12) {
      addAnchorActive(idx + 4);
    }
    else {
      addAnchorActive(idx);
    }
  }
});

// right menu hover
$('.right-menu .rm-center .rmc-center-wrap .rmc-menu .rmc-itm').on('mouseover', function() {
  if($(this).hasClass('active')) {} else {
    // $('.right-menu .rm-center .rmc-center-wrap .rmc-menu .rmc-itm').removeClass('active');
    $(this).addClass('active');

    // $('.right-menu .rm-center .rmc-center-wrap .rmc-menu .rmc-itm ul').slideUp(600);
    $('.right-menu .rm-center .rmc-center-wrap .rmc-menu .rmc-itm.active ul').slideDown(600);
  }
});
$('.right-menu .rm-center').on('mouseleave', function() {
  $('.right-menu .rm-center .rmc-center-wrap .rmc-menu .rmc-itm').removeClass('active');
  $('.right-menu .rm-center .rmc-center-wrap .rmc-menu .rmc-itm ul').slideUp(600);
});

// change active items on hover desktop
if(isNotMobile) {
  hoverActive('.s-three .st-items .st-itm');
  hoverActive('.s-fond .sf-items .sf-itm');
} else {
  clickActive('.s-three .st-items .st-itm');
  clickActive('.s-fond .sf-items .sf-itm');
}
// click open quest
if(!isNotMobile) {
  $('.quest').click(function(e) {
    e.stopPropagation();
    $('.quest').removeClass('active');
    $(this).addClass('active');
  });
}

// click open logos in adap
if(!isNotMobile) {
  $('.s-partners .spt-content-wrap .spt-content .spt-lg-items .spt-lg-itm').click(function() {
    $('.s-partners .spt-content-wrap .spt-content .spt-lg-items .spt-lg-itm').removeClass('active');
    $(this).addClass('active');
  });

  $('.s-partners .spt-content-wrap .spt-content .spt-txt-items .spt-txt-itm').click(function() {
    if($(this).hasClass('active')) {

    } else {
      $('.s-partners .spt-content-wrap .spt-content .spt-txt-items .spt-txt-itm').removeClass('active');
      $(this).addClass('active');


      $('.s-partners .spt-content-wrap .spt-content .spt-txt-items .spt-txt-itm .spt-t-descr').slideUp();
      $('.s-partners .spt-content-wrap .spt-content .spt-txt-items .spt-txt-itm.active .spt-t-descr').slideDown();
    }
  });
}

// tabs
if(isNotMobile) {
  $('.s-why .sw-nav .sw-nv-itm .sw-nv-descr').click(function() {
    $(this).closest('.sw-nav').find('.sw-nv-itm').removeClass('active');
    $(this).parent().addClass('active');

    $(this).closest('.flex-block').find('.sw-content').hide().eq($(this).parent().index()).fadeIn(1300);
  });
}

changeTabs('.s-partners .sp-top .spt-items .sp-itm', '.s-partners .spt-content-wrap .spt-content');
if(isNotMobile) {
  changeTabs('footer .ft-center .ftc-flex .ftc-nav .ftc-nv-itm', 'footer .ft-center .ftc-flex .ftc-content-wrap .ftc-content');
} else {
  $('footer .ft-center .ftc-flex .ftc-nav .ftc-nv-itm').click(function() {
    $('footer .ft-center .ftc-flex .ftc-nav .ftc-nv-itm').removeClass('active');
    $(this).addClass('active');
  });
}


$('.s-graphics .sg-top .sg-nav .sg-nv-itm').click(function() {
  $('.s-graphics .sg-top .sg-nav .sg-nv-itm').removeClass('active');
  $(this).addClass('active');

  // $('.s-graphics .sg-content-wrap .sg-content').removeClass('active').eq($(this).index()).addClass('active');
  $('.s-graphics .sg-content-wrap .sg-content').hide().eq($(this).index()).fadeIn(1300);
  $('.s-graphics .sg-chart-wrap .sg-chart').removeClass('active').eq($(this).index()).addClass('active');
});


// swiper sliders
const teamSlider = new Swiper('.s-team .swiper-container', {
  slidesPerView: 'auto',
  spaceBetween: 0,
  pagination: {
    el: '.s-team .swiper-pagination',
    clickable: true,
  },
});

const newsSlider = new Swiper('.s-news .swiper-container', {
  slidesPerView: 2,
  spaceBetween: 52,
  pagination: {
    el: '.s-news .swiper-pagination',
    clickable: true,
  },
  breakpoints: {
    1600: {
      spaceBetween: 42,
    },
    1440: {
      spaceBetween: 38,
    },
    1024: {
      spaceBetween: 32,
    },
    480: {
      slidesPerView: 1,
    }
  }
});

const mobileNavSlider = new Swiper('.mobile-menu .swiper-container', {
  slidesPerView: 'auto',
  spaceBetween: 12,
});
setTimeout(() => {
  mobileNavSlider.update()
}, 300);

if($('.s-why .sw-mobile-slider .swiper-container').length) {
  document.querySelectorAll('.s-why .sw-mobile-slider .swiper-container').forEach((sldr) => {
    const slider = new Swiper(sldr, {
      slidesPerView: 'auto',
      spaceBetween: 27,
    });

    setTimeout(() => {
      slider.update()
    }, 300);
  })
}

// custom popup
$('[data-popup]').click(function(e) {
  e.preventDefault();
  const id = $(this).attr('data-popup');
  const isMobilePopupOnly = $(this).hasClass('mobile-popup-only') && isNotMobile;

  if(!isMobilePopupOnly) {
    $('.custom-popup').removeClass('active');
    $(`#${id}, .close-custom-popup`).addClass('active');
    // disable scroll
    $.fn.fullpage.setAllowScrolling(false);
  }
  
});
$('.close-custom-popup').click(function() {
  $('.custom-popup').removeClass('active');
  $(this).removeClass('active');
  $.fn.fullpage.setAllowScrolling(true);
});

$('body').click(function() {
  $('.quest').removeClass('active');
  $('.burger-menu').removeClass('active');
});

// open burger mnu
$('.right-menu .rm-top .rmt-burger, header .hd-right .hd-burger').click(function(e) {
  e.stopPropagation();
  $('.burger-menu').addClass('active');
});
$('.burger-menu').click(function(e) {
  e.stopPropagation();
});
$('.burger-menu .bm-top .bm-close, .burger-menu .bm-mid .bmm-col .bmm-ul li a').click(function(e) {
  $('.burger-menu').removeClass('active');
});