import fs from 'fs';
import fonter from 'gulp-fonter';
import ttf2woff2 from 'gulp-ttf2woff2';


export const otfToTtf = () => {
	// Ищем файлы шрифтов .otf конвертируем в ttf и кидаем в fonts
	return app.gulp.src(`${app.path.srcFolder}/fonts/*.otf`, {})
		.pipe(app.plugins.plumber(
			app.plugins.notify.onError({
				title: 'FONTS',
				message: 'Erorr: <%= error.message %>'
			}))
		)
		.pipe(fonter({
			formats: ['ttf']
		}))
		.pipe(app.gulp.dest(`${app.path.srcFolder}/fonts/`))
}

export const ttfToWoff = () => {
	// Ищем файлы шрифтов .ttf конвертируем в woff и кидаем в fonts. Ищем ttf кидаем в woff и в результат
	return app.gulp.src(`${app.path.srcFolder}/fonts/*.ttf`, {})
		.pipe(app.plugins.plumber(
			app.plugins.notify.onError({
				title: 'FONTS',
				message: 'Erorr: <%= error.message %>'
			}))
		)
		.pipe(fonter({
			formats: ['woff']
		}))
		.pipe(app.gulp.dest(`${app.path.build.fonts}`))
		.pipe(app.gulp.src(`${app.path.srcFolder}/fonts/*.ttf`))
		.pipe(ttf2woff2())
		.pipe(app.gulp.dest(`${app.path.build.fonts}`))
}

export const fontsStyle = () => {
	const fontsFile = `${app.path.srcFolder}/sass/fonts.sass`;

	fs.readdir(app.path.build.fonts, function(err, fontsFiles) {
		if(fontsFiles) {
			if(!fs.existsSync(fontsFile)) {
				fs.writeFile(fontsFile, '', cb)
				let newFileOnly;
				for(var i = 0; i < fontsFiles.length; i++) {
					let fontFileName = fontsFiles[i].split('.')[0];
					if(newFileOnly !== fontFileName) {
						let fontName = fontFileName.split('-')[0] ? fontFileName.split('-')[0] : fontFileName;
						let fontWeight = fontFileName.split('-')[1] ? fontFileName.split('-')[1] : fontFileName;

						switch(fontWeight.toLowerCase()) {
							case 'thin':
								fontWeight = 100;
								break;
							case 'extralight':
								fontWeight = 200;
								break;
							case 'light':
								fontWeight = 300;
								break;
							case 'medium':
								fontWeight = 500;
								break;
							case 'semibold':
								fontWeight = 600;
								break;
							case 'bold':
								fontWeight = 700;
								break;
							case 'extrabold' || 'heavy':
								fontWeight = 800;
								break;
							case 'black':
								fontWeight = 900;
								break;
							default:
								fontWeight = 400;
						}

						fs.appendFile(fontsFile,
							`@font-face\r\n	font-family: '${fontName}'\r\n	font-display: swap\r\n	font-weight: ${fontWeight}\r\n	font-style: normal\r\n	src: url('../fonts/${fontFileName}.woff2') format('woff2'), url('../fonts/${fontFileName}.woff') format('woff')\r\n\n`, cb)

						newFileOnly = fontFileName;
					}
				}
			} else {
				console.log('Файл sass/fonts.sass уже существует. Для обновления, его нужно сначала удалить')
			}
		}
	});

	return app.gulp.src(`${app.path.srcFolder}`);
	function cb() {}
}